let toDoId = 1

export function addTask(value) {
  return {
    type: 'ADD_TASK',
    id: toDoId++,
    text: value
  }
}

export function changeAction(e) {
  let value = e.target.value
  return {
    type: 'INPUT_CHANGE',
    payload: value
  }
}

export function checkedAction(e, id) {
  const value = e.target.checked
  return {
    type: 'CHECKBOX_CHECK',
    payload: value,
    id
  }
}
