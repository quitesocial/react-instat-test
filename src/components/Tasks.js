import React from 'react'

import TaskContainer from '../containers/Task'

const Tasks = (props) => {
  const { tasks } = props
  return tasks.map(task => {
    return <TaskContainer key={task.id} task={task} />
  })
}

export default Tasks
