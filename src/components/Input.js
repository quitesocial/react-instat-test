import React from 'react'

const Input = (props) => {
  const { changeAction, addTask, inputValue } = props
  return <input type="text"
                className="todo__input"
                placeholder="What do you want to do?"
                onChange={(e) => changeAction(e)}
                onKeyPress={(target) => target.charCode === 13 ? addTask(inputValue) : null} />
}

export default Input
