import React from 'react'

const Task = (props) => {
  const { task, checkedAction } = props
  return <li
      key={task.id}
      className={'todo__item' + (task.completed ? ' todo__item_disabled' : '')}>
      <span className="todo__num">{task.id}</span>
      <span className="todo__text">{task.text}</span>
      <span className={'todo__status' + (task.completed ? ' todo__status_done' : ' todo__status_progress')}>
          {task.completed ? 'done' : 'in progress'}
        </span>
      <input
        type="checkbox"
        className="todo__checkbox"
        onChange={(e) => checkedAction(e, task.id)}/>
    </li>
}

export default Task
