import React, { Fragment } from 'react'
import html2canvas from 'html2canvas'


import InputContainer from '../containers/Input'
import TasksContainer from '../containers/Tasks'

const App = () => {
  const takeScreenshot = (e) => {
    e.preventDefault()
    html2canvas(e.currentTarget)
      .then((canvas) => {
        const base64Image = canvas.toDataURL().split(';base64,').pop();
        document.getElementById('image').setAttribute('href', canvas.toDataURL())
        document.getElementById('image').setAttribute('download', base64Image)
      })
  }
  return <Fragment>
    <div className="todo" onClick={e => takeScreenshot(e)}>
      <InputContainer />
      <span className="todo__tip">Press ENTER to add new task</span>
      <ul className="todo__list">
        <TasksContainer />
      </ul>
    </div>
    <a id="image">Download Image</a>
  </Fragment>
}

export default App
