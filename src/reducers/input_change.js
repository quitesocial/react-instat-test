export default function (state={}, action) {
  switch(action.type) {
    case 'INPUT_CHANGE':
      return {
        ...state,
        value: action.payload
      }
    default:
      return state
  }
}
