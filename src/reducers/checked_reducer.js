export default function (state={}, action) {
  switch(action.type) {
    case 'CHECKED_CHECK':
      return {
        ...state,
        value: action.payload
      }
    default:
      return state
  }
}
