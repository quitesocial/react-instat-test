import { combineReducers } from 'redux'

import inputValue from './input_change'
import tasks from './task_reducer'

export default combineReducers({
  inputValue,
  tasks
})
