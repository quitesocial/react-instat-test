export default function (state=[], action) {
  switch(action.type) {
    case 'ADD_TASK':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ]
    case 'CHECKBOX_CHECK':
      return state.map(todo =>
        (todo.id === action.id) ? {...todo, completed: !todo.completed} : todo
      )
    default:
      return state
  }
}
