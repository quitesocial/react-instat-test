import React, { Component } from 'react'
import { connect } from 'react-redux'

import { addTask, changeAction } from '../actions/index';
import Input from '../components/Input'

class InputContainer extends Component {

  render() {
    console.log('input rendered')
    return <Input {...this.props} />
  }
}

function mapStateToProps(state) {
  return {
    inputValue: state.inputValue.value
  }
}

export default connect(mapStateToProps, { addTask, changeAction })(InputContainer)
