import React, { Component } from 'react'
import { connect } from 'react-redux'

import { checkedAction } from '../actions/index';
import Task from '../components/Task'

class TaskContainer extends Component {

  render() {
    console.log('one task rendered')
    return <Task {...this.props} />
  }
}

export default connect(null, { checkedAction })(TaskContainer)
