import React, { Component } from 'react'
import { connect } from 'react-redux'

import Tasks from '../components/Tasks'

class TasksContainer extends Component {

  render() {
    console.log('tasks rendered')
    return <Tasks {...this.props} />
  }
}

function mapStateToProps(state) {
  return {
    tasks: state.tasks
  }
}

export default connect(mapStateToProps, null)(TasksContainer)
